---
title: C/С++ проект с системами сборки CMake + Ninja в NetBeans
date: 2017-10-22 20:22:07
tags:
- netbeans
- cmake
- ninja
- cpp
categories:
- IDE
---
[NetBeans](https://netbeans.org/) — отличная и удобная среда для разработки проектов на разных языках программирования.

Учась на 3-м курсе в университете, я писал небольшие программы, которые были разбиты на несколько блоков.
По объёму программы небольшие, но в каждой категории их несколько.

Дабы было легко их собирать я решил воспользоваться системой сборки и моим выбором стал [CMake](https://cmake.org/). Эта штука довольная мощная и к её синтаксису я сразу приноровился.

По умолчанию она генерирует `Makefile`, который уже медленный и неэффективный.
Однако есть возможность генерировать файлы сборки других систем, одна из таких — [Ninja](https://ninja-build.org).

Ниндзя собирает довольно быстро и эффективно.
Очень хотелось бы легко собирать с его помощью в какой-нибудь среде разработки.
Под рукой был NetBeans и он умеет создавать CMake-проекты с Makefile'ом.
Всё, что нужно сделать, — это подправить некоторые опции конфигурации при создании проекта.

Приступим!

**1.** Создадим проект.

![](/img/for_posts/netbeans-cmake-01.png)

**2.** В категории «C/C++» выбираем «Проект на C/C++ с существующими исходными файлами».

![](/img/for_posts/netbeans-cmake-02.png)

**3.** Укажем папку с исходниками, а режим настройки переключим на «Выборочно».

![](/img/for_posts/netbeans-cmake-03.png)

**4.** На шаге «Действие предварительной сборки» должен быть отмечен флажок «Требуется действие предварительной сборки».
Также нужно выбрать опцию «Предварительно настроенная команда».
А в поле «Аргументы» нужно изменить выдеделенную часть `"Unix Makefiles"`:

![](/img/for_posts/netbeans-cmake-04.png)

на `"Ninja"` и последнюю точку — на две точки:

![](/img/for_posts/netbeans-cmake-05.png)
![](/img/for_posts/netbeans-cmake-06.png)

**5.** На шаге «Действия сборки» к «рабочей папке» нужно в конец приписать `/build`.
Это та папка, куда будут скадываться генерируемые файлы.

* Команда очистки: `ninja -t clean`
* Команда построения: `ninja`.

![](/img/for_posts/netbeans-cmake-07.png)

**6.** Просто переходим далее.

![](/img/for_posts/netbeans-cmake-08.png)

**7.** Здесь «Автоматическая настройка», ничего более.

![](/img/for_posts/netbeans-cmake-09.png)

**8.** Имя и расположение вполне устраивает.

![](/img/for_posts/netbeans-cmake-10.png)

Каркас проекта готов. Осталось подправить несколько деталей в настройках проекта.

**1.** Слева во вкладке «Проекты» щёлкаем правой кнопкой по папке с именем проекта и в выпадающем меню выбираем «Свойства».

![](/img/for_posts/netbeans-cmake-11.png)

**2.** Слева в меню «Категории» выбирает подпункт «Предварительная сборка».
Справа настраивает параметры:

* Рабочий католог — `build`;
* Отмечаем флажок «Сначала предварительная сборка». Т.е. должен сначала CMake сгенерировать файлы сборки.

![](/img/for_posts/netbeans-cmake-12.png)

**3.** Далее, нужно непосредственно создать рабочий католог `build`.
Я предпочитаю быстро вбить команду в терминале:

![](/img/for_posts/netbeans-cmake-13.png)

Основные приготовления завершены!

Теперь можно протестировать сборку.

Правой кнопкой по строчке с именем проекта и выбираем «Очистить и собрать»:

![](/img/for_posts/netbeans-cmake-14.png)

Спустя мгновение в панели вывода сообщений должна красоваться зелёная надпись “<span style="color: #008000;">SUCCESSFUL</span>”:

![](/img/for_posts/netbeans-cmake-15.png)

Теперь в папке сборки появились подпапки, которые совпадают с теми, которые были обработаны CMake'ом.
Там и находятся собранные исполняемые файлы, которые мы можем выбрать правой кнопкой и запустить на выполнение:

![](/img/for_posts/netbeans-cmake-16.png)

В окне «Выполнение» из списка «Проект» выбираем название текущего:

![](/img/for_posts/netbeans-cmake-17.png)

Вот и всё! Программа запускается в терминале!

В итоге мне удалось настроить NetBeans, чтобы он успешно собирал проект с системами сборки CMake и Ninja!
