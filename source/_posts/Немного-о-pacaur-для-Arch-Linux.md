---
title: Немного о pacaur для Arch Linux
date: 2017-02-21 12:30:53
tags:
- linux
- arch
- pacaur
- cower
categories:
- Linux
---
Существует в среде [Arch Linux](https://www.archlinux.org/) такой репозиторий как [AUR](https://aur.archlinux.org/). Прочитаем [его описание](https://wiki.archlinux.org/index.php/Arch_User_Repository_%28Русский%29) на официальном [Arch Wiki](https://wiki.archlinux.org/):

> Пользовательский репозиторий Arch (Arch User Repository, `AUR`) — это поддерживаемое сообществом хранилище ПО для пользователей Arch. Он содержит описания пакетов (файлы [`PKGBUILD`](https://wiki.archlinux.org/index.php/PKGBUILD)), которые позволят вам скомпилировать пакет из исходников с помощью [`makepkg`](https://wiki.archlinux.org/index.php/Makepkg) и затем установить его, используя [`pacman`](https://wiki.archlinux.org/index.php/Pacman). `AUR` был создан, чтобы объединить и предоставить в общий доступ новые пакеты от сообщества и способствовать скорому включению популярных пакетов в репозиторий [`community`](https://www.archlinux.org/packages/?sort=&repo=Community).

Лучше и не скажешь! И действительно, часто разработчики программ не могут или не умеют собирать под свою программу пакет, потому что это довольно нетривиально и сложно (в Debian особенно). Лучшим выходом является, по возможности, написать простой сценарий компиляции и установки программы в систему, не собирая бинарный пакет самому программисту, за него это будут делать конечные пользователи.

Для облегчения поиска, компиляции и установки скриптов из AUR есть специальные утилиты-помощники. Популярной такой утилитой в своё время был (и, наверное, остаётся) [`yaourt`](https://archlinux.fr/yaourt-en) (или, по-русски, «йогурт»). Первое время я им тоже пользовался, но несколько месяцев назад я сменил её на [`pacaur`](https://github.com/rmarquis/pacaur), позже расскажу почему.

`pacaur` использует [`cower`](https://github.com/falconindy/cower) как помощника загрузки, проверки обновлений и зависимостей. Установим сначала `cower`:

```
mkdir -p d/cower
cd d/cower
curl -o PKGBUILD https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=cower-git
makepkg -si
```

Замечу, что пакет [`pkgtools`](https://aur.archlinux.org/packages/pkgtools/) должен быть установлен, чтобы был `makepkg`.

Теперь с помощью свежеустановленного `cower` установим сам `pacaur`:

```
cd ..
cower -d pacaur-git
cd pacaur-git
makepkg -si
```

Установили! Скопируем в домашнюю директорию конфиги:

```
mkdir ~/.config/pacaur
sudo cp /etc/xdg/pacaur/config $HOME/.config/pacaur/config
sudo chown <user>:users $HOME/.config/pacaur/config # вместо <user> ввести имя пользователя
```

Вот пример моих настроек:

![](/img/for_posts/pacaur_and_cower_configs.png)

Опции у `pacaur` хорошо описаны, но есть такая хорошая опция как `sudoloop`, которая не позволяет каждый раз вводить пароль после того, как закончится действие `sudo`. Именно эта фича мне понравилась в `pacaur` и которой нет в `yaourt`.

Для тех, кто привык писать `yaourt`, можно прописать алиас в `~/.bashrc`:

```
alias pacaur='pacaur --noconfirm --noedit --ignore-ood'
alias yaourt='pacaur'
```

Нужно не забыть после этого сделать:

```
source ~/.bashrc
```

Чтобы изменения настроек `bash` вступили в силу.

Ну вот и всё! `pacaur` установлен и настроен!
