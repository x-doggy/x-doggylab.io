---
title: Нетривиальная установка Microsoft R Open и R Commander в Arch Linux
date: 2018-11-03 23:55:23
tags:
- R
- Arch
categories:
- Linux
---

Недавно я [рассказывал](/2018/09/19/Установка-Microsoft-R-Open-и-R-Commander-в-Fedora-28/) как установить набор программ для работы со статистикой на языке R в имплементации Microsoft в Fedora GNU/Linux.

Однако, с недавней [новостью](http://www.opennet.ru/opennews/art.shtml?num=49515) о покупке Red Hat'а компанией IBM и одновременно неудачным апгрейдом до Fedora 29 пришлось перейти обратно на родной Arch. К тому же будущее как Red Hat, так и дистрибутива Fedora видится мутным.

Arch, как известно [rolling release-дистрибутив](https://wiki.archlinux.org/index.php/Frequently_asked_questions#Is_Arch_Linux_a_stable_distribution.3F_Will_I_get_frequent_breakage.3F), т.е. нет разделения на релизы, а пакеты этого дистрибутива становятся доступными сразу как их соберет мэйнтейнер. И, в связи с этим, пакет `Rcmdr` в рамках среды `R` не собирается правильно из-за несовместимости в зависимом пакете `later`. [Проблема](https://github.com/r-lib/later/issues/45) состоит в том, что старые версии этого пакета неверно собраются из-под окружения `glibc` новых версий. Пропатченная версия доступна из Github, но недоступна из репозитория R-пакетов CRAN. Поэтому этой заметкой мне надлежит поставить правильную версию этого пакета и успешно установить тем самым `R Commander`.

Начнём с установки окружения Microsoft R Open. К большому счастью, пакет [`microsoft-r-open`]() доступен из [пользовательского репозитория](/2017/02/21/Немного-о-pacaur-для-Arch-Linux/) Arch Linux (AUR). К тому же, там верно прописаны зависимости и поэтому проблем точно не будет! Поставить его можно с помощью любого [AUR-helper](https://wiki.archlinux.org/index.php/AUR_helpers)'а, я теперь использую [`yay`](https://aur.archlinux.org/packages/yay-bin):

```
yay -S microsoft-r-open
```

После установки пакета аналогично тому, как я делал в Fedora, заходим в R не от своего имени:

```
sudo R
```

Ставим пакет `devtools`:

```
install.packages("devtools")
```

Далее, пакет `later`, патченный из гитхаба, ставится этой командой:

```
devtools::install_github("r-lib/later")
```

Однако, команда эта валится с ошибкой:

> `Installation failed: error setting certificate verify locations:`
>  `CAfile: microsoft-r-cacert.pem`
>  `CApath: none`

Необходимо ее исправить. Благо в гитхабе [открыт](https://github.com/Microsoft/microsoft-r-open/issues/63) issue, посвященный этой проблеме.

Не выходя из консоли R, удаляем пакеты `curl` и `httr`:

```
remove.packages(c("curl","httr"))
```

После этого выходим из R, снова набираем: `sudo R` и заново оказывается в REPL. Ставим пакеты заново:

```
install.packages(c("curl", "httr"))
```

И устанавливаем верное значение переменной окружения R:

```
Sys.setenv(CURL_CA_BUNDLE="/utils/microsoft-r-open-3.4.3/lib64/R/lib/microsoft-r-cacert.pem")
```

И тут же, снова не выходя из консоли R, ставим непосредственно сам `Rcmdr`:

```
install.packages("Rcmdr", dependencies=TRUE)
```

Тем самым, мы подменили зависимость `later`, которая бы подтянулась этой командой, верной и пропатченной. Поэтому во время разрешений зависимостей команда выше пропустит пакет `later`, так как он был установлен заранее.

Вот и всё!

