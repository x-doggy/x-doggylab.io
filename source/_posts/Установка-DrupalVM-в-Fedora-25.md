---
title: Установка DrupalVM в Fedora 25
date: 2017-06-12 15:33:57
tags:
- linux
- drupalvm
- vagrant
- ansible
- virtualbox
- fedora
categories:
- Linux
---

Здесь я опишу основные тонкости установки DrupalVM в Fedora 25 и решения проблем, которые могут возникнуть в процессе настройки.

Первым делом, установим и активируем необходимые зависимости:

```
dnf install nfs-utils libselinux-python
systemctl enable nfs-server
systemctl start nfs-server
systemctl enable rpcbind
systemctl start rpcbind
```

Первым делом, надо установить [VirtualBox](/2017/06/12/Установка-VirtualBox-в-Fedora-25/).

Вторым делом, установим Ansible и [Vagrant](https://www.vagrantup.com/) (Из-под суперюзера).

```
dnf install ansible
```

Чтобы установить Vagrant, с [официального сайта](https://www.vagrantup.com/downloads.html) качаем 64-битную версию для CentOS, устанавливаем и проводим дополнительные манипуляции:

```
dnf install ./vagrant_*x86_64.rpm
getent group vagrant > /dev/null || sudo groupadd -r vagrant
gpasswd -a `whoami` vagrant
newgrp vagrant
vagrant plugin install vagrant-hostsupdater
vagrant plugin install vagrant-vbguest
```

OK, теперь надо выбрать какую-нибудь директорию и туда склонировать [DrupalVM](https://www.drupalvm.com/). Идём по ссылке на оф. сайт и жмём __“Download Drupal VM”.__ Распаковываем архив в выбранную вами папку. В терминале сделайте ``cd`` на эту папку.

Создадим конфиги из уже готовых примеров:
 
```
cp default.config.yml config.yml
cp example.drupal.composer.json drupal.composer.json
```

По своему усмотрению нужно настроить ``config.yml``.

Теперь по идее нужно сделать ``vagrant up``, но нет, нужно освободить программы от запретов системы:

```
setsebool -P virt_use_nfs 1
firewall-cmd --change-inteface=vboxnet0
firewall-cmd --zone=internal --permanent --add-service=nfs
firewall-cmd --zone=internal --permanent --add-service=rpc-bind
firewall-cmd --zone=internal --permanent --add-service=mountd
firewall-cmd --zone=internal --permanent --add-port=2049/udp
firewall-cmd --reload
ip a add 192.168.88.0/255.255.255.0 dev vboxnet0
ip link set vboxnet0 up
```

__TODO:__ Дополнить статью советами по ссылкам:

* [https://developer.fedoraproject.org/tools/vagrant/vagrant-nfs.html](https://developer.fedoraproject.org/tools/vagrant/vagrant-nfs.html)
* [http://www.server-world.info/en/note?os=Fedora_25&p=nfs](http://www.server-world.info/en/note?os=Fedora_25&p=nfs)
* [https://fconfig.wordpress.com/2006/08/17/setting-up-a-fedora-nfs-server/](https://fconfig.wordpress.com/2006/08/17/setting-up-a-fedora-nfs-server/)
* [https://niklan.net/147](https://niklan.net/147)
* [https://web.archive.org/web/20150706105420/http:/.bak1an.so/2014/03/23/fedora-vagrant-nfs/](https://web.archive.org/web/20150706105420/http:/.bak1an.so/2014/03/23/fedora-vagrant-nfs/)