---
title: Установить двухпанельный файловый менеджер Double Commander в Fedora 20
date: 2014-11-06 22:42:05
tags:
- linux
- doublecmd
- fedora
categories:
- Linux
---
Установив Федору, не мог найти пакет ``doublecmd`` в официальном репозитории Fedora и даже в RPMFusion. Решение нашлось довольно быстро — в отдельном репозитории OpenSUSE. Всего три простых команды для установки:

```
cd /etc/yum.repos.d/
wget http://download.opensuse.org/repositories/home:Alexx2000/Fedora_20/home:Alexx2000.repo
sudo yum install doublecmd-qt
```

**Источник:** [software.opensuse.org](http://software.opensuse.org/download.html?project=home%3AAlexx2000&package=doublecmd-qt)

**P.S.** Там же можно найти команды для установки в OpenSUSE, Debian и Ubuntu.