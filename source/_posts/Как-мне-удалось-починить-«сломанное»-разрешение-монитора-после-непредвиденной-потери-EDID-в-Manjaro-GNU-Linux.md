---
title: Как мне удалось починить «сломанное» разрешение монитора после непредвиденной потери EDID в Manjaro GNU/Linux
date: 2016-02-21 14:58:28
tags:
- EDID
- linux
- manjaro
categories:
- Linux
---
Однажды после обновления системы и перезагрузки компьютера, я заметил, что разрешение монитора снизилось с 1400×900 до 1024×768, причём в настройках дисплея Xfce максимально доступным было разрешение 1024. Кроме того, на стадии загрузки systemd вылетало несколько сообщений вида:

> nouveau 0000:01:00.0: DRM: DDC responded, but no EDID for VGA-1

После некоторого гугления в багтрекере на ланчпаде я нашёл параметры того же, что и у меня монитора, взятые из конфига драйвера nvidia. Они были в точности такими:

> Ubuntu 8.04
> nvidia-glx-new
> Nvidia Fx 5200
> 1440x900 native monitor resolution.

> Monitor Specs:

> ModelName "ViewSonic VA1916wSERIES"
> HorizSync 24.0 - 82.0
> VertRefresh 50.0 - 75.0

> 1440 x 900 @ 60, 75 Hz
> 1280 x 1024 @ 60, 75 Hz
> 1024 x 768 @ 60, 70, 72, 75 Hz
> 800 x 600 @ 56, 60, 72, 75 Hz
> 640 x 480 @ 60, 75 Hz
> 720 x 400 @ 70 Hz

На другом форуме я нашёл готовый конфиг для другого монитора и он был в точности таким:

```
Section "Monitor"
    Identifier     "Monitor0"
    VendorName     "Philips"
    ModelName      "193V5L"
    HorizSync       30.0 - 83.0
    VertRefresh     56.0 - 75.0
    Modeline       "1366x768_59.8"   85.50  1366 1436 1579 1792  768 771 774 798 +hsync +vsync
    Option         "UseEDID=false"
    Option         "DPMS"
    Option         "PreferredMode" "1366x768"
EndSection
```

Несложно увидеть, что это секция конфига файла _/etc/X11/xorg.conf_ в юниксах. Значит, нужно «совместить» два этих конфига и прописать полученное куда?.. Правильно, в _/etc/X11/mhwd.d/nouveau.conf_, потому что в манджаро нежелательны конфиги xorg, вместо этого есть клёвая штучка [mhwd](https://wiki.manjaro.org/index.php/Manjaro_Hardware_Detection).

Как совместить я узнал отсюда: [http://www.freebsd.org/doc/ru/books/handbook/x-config.html](http://www.freebsd.org/doc/ru/books/handbook/x-config.html). Параметры конкретно вашего монитора должны сохраниться в файле _/var/log/Xorg.0.log_. В итоге получился вот такой конфиг:

```
Section "Monitor"
    Identifier     "Monitor0"
    VendorName     "ViewSonic"
    ModelName      "VA1916wSERIES"
    HorizSync       24.0 - 82.0
    VertRefresh     50.0 - 75.0
    Modeline       "1440x900_60"   106.50  1400 1520 1672 1904  900 903 909 934 +hsync +vsync
    Option         "UseEDID=false"
    Option         "DPMS"
    Option         "PreferredMode" "1400x900"
EndSection
```

И вот, после перезагрузки всё заработало!