---
title: >-
  Установить genius_lyrics — консольный поисковик текстов песен на genius.com —
  в Arch Linux
date: 2016-06-18 01:21:15
tags:
- linux
- arch
- genius
categories:
- Linux
---
* Установим необходимые зависимости для работы скрипта:

```
sudo pacman -S python-lxml python-beautifulsoup4
```

* Переходим на страничку на гитхабе: [https://github.com/donniebishop/genius_lyrics](https://github.com/donniebishop/genius_lyrics)

* Скачиваем архив:

![](/img/for_posts/genius_lyrics_github.png)

* Распаковываем архив:

```
7z x genius_lyrics-master.zip
```

* Переместим получившуюся папку в системную директорию:

```
sudo mv genius_lyrics-master/ /usr/local/bin/genius_lyrics/
```

* Для доступа к этой программе без перехода в эту директорию создадим символическую ссылку:

```
sudo ln -s /usr/local/bin/genius_lyrics/genius_lyrics.py /usr/local/bin/glyrics
```

* Всё! Теперь можем искать тексты песен:

```
glyrics -s "Snoop Dogg Gangbangin' 101"
```