---
title: Устанавливаем CLion ещё раз! В Arch Linux!
date: 2016-07-27 20:52:15
tags:
- linux
- clion
- arch
- ide
categories:
- Linux
- IDE
---
Также как и в {% post_link Установить-CLion-новую-среду-разработки-на-C-C-в-Debian-Wheezy-stable прошлый раз %} нам необходимо пройти по ссылке:
[https://confluence.jetbrains.com/display/CLION/Early+Access+Program](https://confluence.jetbrains.com/display/CLION/Early+Access+Program)
и кликнуть по ссылке в разделе загрузки для Linux:

![](/img/for_posts/clion01.png)

Полученный архив той же командой распаковываем:

```
sudo tar xvfz clion-*.gz -C /opt/ &amp;&amp; rm clion-*.gz
```

Переходим в каталог с установщиком:

```
cd /opt/clion/bin/
```

Устанавливаем права на запуск и запускаем установочный скрипт:

```
sudo chmod +x ./clion.sh &amp;&amp; ./clion.sh
```

Следуйте простым инструкциям по установке:

![](/img/for_posts/clion_again_01.png)

Знакомое диалоговое окно. Мы устанавливаем CLion в первый раз, поэтому жмём второй пункт.

![](/img/for_posts/clion_again_02.png)

За хороший программный продукт не грех и заплатить. Если денег нет, то выбираем _“Evaluate for free”_ для активации бесплатного 30-дневного использования.

![](/img/for_posts/clion_again_03.png)

В окне выбора тем выбирайте что вам нравится. Мне нравится по-прежнему _Darcula_.

![](/img/for_posts/clion_again_04.png)

Здесь нужно лишь убедиться, что все галочки напротив всяких _CMake, C++_ и _GDB_ стоят.

![](/img/for_posts/clion_again_05.png)

Это окно настроки дополнительных компонентов, которые могут пригодиться в CLion. Жмём _“Customize”_ в пункте _"Version Controls"._

![](/img/for_posts/clion_again_06.png)

Я лично пользуюсь системой контроля версий _Mercurial,_ а в дальнейшем собираюсь попрактиковать и Git. Поэтому в данном случае будут актуальны эти галочки. Жмём _“Save changes and Go back”_ и возвращаемся в прежний диалог.

![](/img/for_posts/clion_again_07.png)

На вкладке _“Web Development”_ я убрал галочку _“REST client”,_ потому что не знаю что это такое.

![](/img/for_posts/clion_again_08.png)

На вкладке _“Other tools”_ я убрал галочку на пункте _“XSLT и XPath”_, поскольку они мне не нужны. Аналогично применяем изменения и возвращаемся.

![](/img/for_posts/clion_again_09.png)

Это окно установки плагинов. Ничего из этого мне не надо. Пропускаем.

![](/img/for_posts/clion_again_10.png)

В окне создания значка для меню я обычно устанавливаю галку _«Для всех пользователей»._

![](/img/for_posts/clion_again_11.png)

Если вы тоже выбрали этот пункт, то введите пароль пользователя _root_ в окне.

Вот и всё! Можете с удовольствием пользоваться CLion!

![](/img/for_posts/clion_again_12.png)