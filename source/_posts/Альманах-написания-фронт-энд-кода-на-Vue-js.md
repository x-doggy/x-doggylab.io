---
title: Альманах написания фронт-энд кода на Vue.js
date: 2019-12-08 04:37:41
tags:
---

### Фронт-энд на Vue.js

1. Использовать TypeScript всегда. Позволяет избегать type mismatching в коде.

2. Использовать правило trailing commas для ESLint, т. е. т. н. «висячие запятые» (см. п. «Декларирование компонентов»).
3. Декларирование пропсов. С маленькой буквы и через тире, если название многосложное.

```vue
@Prop("quality-series")
private readonly QualitySeries!: Array<any>;
```

4. Декларирование событий. Аналогично — с маленькой буквы и через дефис.

```vue
this.$emit("date-changed", date);
```

```vue
@date-changed="HandleChangeDate"
```

5. Базовая структура компонента:

```vue
<template>
</template>

<script lang="ts">
import Vue from "vue";
import Component from "vue-class-component";

@Component
export default class NodeActions extends Vue {}
</script>

<style scoped lang="scss">
</style>
```

6. Декларирование компонентов. Указывать явно имя компонента.

```vue
@Component({
   components: {
       "node-editing-dialog": NodeEditingDialog,
   },
})
```

7. Компоненты основных страниц выносить в папку `views/`, в нем не должно содержаться никакой логики, только объявление компонента. Данный компонент должен выводить другой компонент, который располагается в папке `components/` и уже содержит необходимую логику.

**/views/node/NodeListView.vue:**

```vue
<template>
    <node-list-view />
</template>

<script lang="ts">
import Vue from "vue";
import Component from "vue-class-component";
import NodeListView from "@/components/app/node/NodeListView.vue";

@Component({
   components: {
       "node-list-view": NodeListView,
   },
})
export default class NodeActions extends Vue {}
</script>
```

8. Диалоги выносить в отдельный компонент. Таковой из [Vuetify](https://vuetifyjs.com/) ([v-dialog](https://vuetifyjs.com/components/dialogs)), например, содержит слот-активатор, с помощью которого можно вывести компонент, его активирующий.

9. Не использовать [@Watch](https://vuejs.org/v2/guide/computed.html#Watchers), если можно обойтись без него. Попробовать использовать [вычисляемые свойства](https://vuejs.org/v2/guide/computed.html#Computed-Properties).

### Использование хранилища Vuex.

1. Прежде всего, все-таки его использовать. Эта реализация паттерна Flux значительнее понятная, чем таковым является Redux в экосистеме React.

2. Компонент, если использует store, должен выводить не State, а смежный ему Getter.

3. __(Если без TypeScript).__ Использовать map*-процедуры для проброса Vuex-единиц в компонент. При пробросе указывать имена свойств явно.

```vue
import { mapGetters, mapActions, mapMutations } from "vuex";
computed: {
   ...mapGetters("app/nodes", {
       node: "Node",
       lastData: "LastData",
   }),
},
methods: {
   ...mapActions("app/nodes", {
       getNodes: "fetchNodes",
   }),
   ...mapMutations("app/nodes", {
       updateNodeInstantStatus: "updateNodeInstantStatus",
   }),
},
```

4. Для работы с Vuex в компонентах использовать следующие модули: [vuex-class](https://www.npmjs.com/package/vuex-class), [vue-class-component](https://www.npmjs.com/package/vue-class-component), [vue-property-decorator](https://www.npmjs.com/package/vue-property-decorator).

### Mounted-процедура.

1. Всегда делать [mounted](https://vuejs.org/v2/api/#mounted)-процедуру асинхронной, т. к. часто нужно вызывать асинхронные методы именно там.

2. Как только вы написали [mounted](https://vuejs.org/v2/api/#mounted)-логику, подумайте какие данные могут остаться в памяти или какие обработчики могли установиться. Необходимо удалить подобного рода ссылки через [`beforeDestroy`](https://vuejs.org/v2/api/#beforeDestroy). Если на что-то подписались — отпишитесь, навесили обработчик — удалите обработчик, и т. д.

### Другой JavaScript

12. Используйте [async](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/async_function) / [await](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/await), а не [Promise](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise) с его [.then](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise/then), [.catch](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise/catch) и [.finally](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Promise/finally).

13. Старайтесь всегда объявлять переменные через `const`, а не через `let` и `var`. Про `var` вообще забудьте. А если хочется написать `let` — подумайте как это стоит переписать на `const`. Я молчу про типовые примеры использования `let`, например, в циклах.

14. Не используйте цикл `for` для итерации по индексам, если это не типовой пример какой-нибудь сортировки из учебника. Ваши друзья в этом деле:
[.forEach](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach), [.map](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/map), [.flatMap](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/flatMap), [.reduce](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce), [.find](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/find), [.findIndex](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex), [.some](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/some), [.every](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/every), [.includes](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/includes).

15. Избегайте [.push](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/push), [.unshift](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift) и прочее мутирование массива по ссылке. Реактивные данные во Vue меняются через [иммутабельные структуры](https://vuejs.org/v2/guide/list.html#Maintaining-State). Заместо [.push](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/push) — иммутабельный [.concat](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/concat), а для [.unshift](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift) у массива или мутирования объекта — [spread](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Operators/Spread_syntax)-оператор и [Object.assign](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/assign).

### Паттерны и классы со сторонней логикой.

1. Избегать использования `null` и `undefined`. `NULL` вообще в программировании является «[The Billion Dollar Mistake](https://www.yegor256.com/2014/05/13/why-null-is-bad.html)», причиной `NullPointerException` в Java и `«typeof x is undefined»` в JavaScript. Подходящий паттерн для замены — [Null Object](https://en.wikipedia.org/wiki/Null_object_pattern).

2. [Имя класса не должно заканчиваться на -or, -er](https://www.yegor256.com/2015/03/09/objects-end-with-er.html). Никаких «парсеров», «калькуляторов», «контроллеров», «диспатчеров», «коллекторов», «сэндеров», «хэндлеров», «локаторов», если вы не пишете код в процедурном стиле.

3. **SOLID.** Следуйте максимально этим принципам.

4. **Не попадитесь на ловушку синглтона!** Это своеобразная попытка не решить проблему глобальных переменных, а замаскировать это дело в класс с чудесным поведением, в котором ни один из SOLID не соблюден и который с виду даже выглядит как дурак:

** Пример на Java: **

```java
public class Singleton {
   private static volatile Singleton instance;
   private Singleton() {}
   public static Singleton getInstance() {
       if (instance == null) {
           synchronized (Singleton.class) {
               if (instance == null) instance = new Singleton();
          }
       }
   }
}
```

Используйте [Dependency Injection](https://martinfowler.com/articles/injection.html) в ваших классах.

5. **Не используйте [гэттеры, сэттеры](https://www.yegor256.com/2014/09/16/getters-and-setters-are-evil.html) и static-методы в классах.** Доверьте объекту решение проблемы, сконструируйте его сразу в нужными параметрами (создайте, если надо, 100 конструкторов) — и сделайте так, чтобы он содержал не более 3-4 методов. Это будет ясный пример использования Dependency Injection, к тому же. Подумайте как объект разбить на составляющие правильно.

6. **Создавайте Immutable классы.** Мутабельные классы [изменяют состояние неявно](https://www.yegor256.com/2014/06/09/objects-should-be-immutable.html). Вместо ``user.setName("Jim");`` делайте ``user = user.rename("Jim");``.

7. **Уходите от [Utility-классов](https://www.yegor256.com/2014/05/05/oop-alternative-to-utility-classes.html).** Это просто смешно, когда есть: [StringUtils](https://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/StringUtils.html), [ArrayUtils](https://commons.apache.org/proper/commons-lang/javadocs/api-3.9/org/apache/commons/lang3/ArrayUtils.html), [BufferUtils](https://commons.apache.org/proper/commons-collections/javadocs/api-3.2.2/org/apache/commons/collections/BufferUtils.html), [MathUtils](https://developer.android.com/reference/android/support/v4/math/MathUtils), [ControllerUtils](https://docs.spring.io/spring-data/rest/docs/current/api/org/springframework/data/rest/webmvc/ControllerUtils.html) и т.п. Создавайте immutable-объект и оборачивайте его через «[декоратор](https://www.yegor256.com/2015/02/26/composable-decorators.html)», если нужно сделать объект умнее.
Хороший пример есть у класса Reader в Java: [https://docs.oracle.com/javase/7/docs/api/java/io/Reader.html](https://docs.oracle.com/javase/7/docs/api/java/io/Reader.html).
Однако же в экосистеме Java есть и обратные примеры. Допустим, StringUtils: [https://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/StringUtils.html](https://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/StringUtils.html).
