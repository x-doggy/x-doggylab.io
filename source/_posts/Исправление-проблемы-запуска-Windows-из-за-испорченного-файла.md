---
title: Исправление проблемы запуска Windows из-за испорченного файла
date: 2013-03-08 13:28:28
tags:
- windows
categories:
- Windows
---
Недавно у моего друга случилась проблема, когда видна слетала и выдавала следующую ошибку:

>Не удается запустить Windows из-за испорченного или отсутствующего файла \WINDOWS\SYSTEM32\CONFIG\SYSTEM. Можно попробовать восстановить этот файл, запустив программу установки windows с оригинального установочного cd-rom, выберите r в первом диалоговом окне

Короче говоря, система слетела и просит вас переустановить саму себя. Скорее всего, вы:

* Неправильно выключали компьютер;
* Наделали проблем с жестким диском;
* Запустили вирус и тот повредил важный системный файл.

Пункт 3 предпочтительнее и он встречается чаще всего.

### Решаем проблему

Во-первых, оговорюсь, что вам понадобится установочный диск в виндой. Без него никак проблему не решить.
Если он у вас есть, то вставьте его в привод, перезагрузитесь. В фазе включения компьютера перезагрузки войдите в БИОС, нажав клавишу `[Delete]`. В разделе загрузки системы выберите такой порядок загрузки, чтобы ваш привод был первым в списке. Выйдите в главное меню, выберите _«Save Settings and Exit»_ (этот пункт может звучать как угодно еще), подтвердите выход.

Далее произойдет запуск с системного диска. После того, как вы немного подождали и появилось сообщение о принятии решений что делать дальше, нажмите клавишу `[r]`. Загрузится темный экран с командной строкой. Там войдите в администратора системы, введите пароль (если он есть), следуя подсказкам системы.

Далее проявите всю свою сноровку, точность и аккуратность, последовательно введя последующие три команды:

```
copy c:\windows\system32\config\system c:\windows\system32\config\system.old
delete c:\windows\system32\config\system
copy c:\windows\repair\system c:\windows\system32\config\system
```

Этими командами мы:

* Переименовали системный файл, чтобы не было конфликта при копировании другого;
* Удалили системную директорию;
* Из папки починки системы восстановили обратно важные системные файлы.

Теперь загрузка должна пойти нормально. После удачного входа в систему переустановите **все** системные драйверы (для мыши, монитора, сетевой платы и т.д.). После этого можно считать, что система удачно восстановлена!