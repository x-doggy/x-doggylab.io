---
title: Установка Bumblebee в Ubuntu 14.04.1/Linux Mint 17.1 с драйвером NVIDIA 331
date: 2015-01-30 15:10:23
tags:
- ubuntu
- mint
- linux
- bumblebee
- nvidia
categories:
- Linux
---
[Bumblebee](https://github.com/Bumblebee-Project/Bumblebee) — это проект по поддержке технологии [NVIDIA Optimus](http://help.ubuntu.ru/wiki/nvidia_optimus) в GNU/Linux системах для сбережения энергии компьютера при работе от аккумулятора.

Вот несколько шагов для полноценной установки:

1. Введите следующие команды:

```
add-apt-repository ppa:xorg-edgers/ppa
add-apt-repository ppa:bumblebee/stable
apt-get update && apt-get dist-upgrade
```

```
apt-get install linux-headers-generic nvidia-331 nvidia-setings bumblebee bumblebee-nvidia bbswitch-dkms primus primus-libs:i386
```

```
usermod -a -G bumblebee <имя юзера> reboot # чтобы перезагрузиться
```


2. Отредактируйте файл ``/etc/bumblebee/bumblebee.conf`` так, чтобы строчки соответствовали:

```
Driver=nvidia

[driver-nvidia]
KernelDriver=nvidia-331
LibraryPath=/usr/lib/nvidia-331:/usr/lib32/nvidia-331 
XorgModulePath=/usr/lib/nvidia-331/xorg,/usr/lib/xorg/modules
```

Вот и всё! Теперь запускать проги можно с префиксом “``optirun``”, типо так:

```
optirun firefox
```

Всё!

По мотивам [techtus.info](https://techtuts.info/2014/06/install-bumblebee-linux-mint-17/).

**P.S.** В русской Ибунту-вики [появилась](http://help.ubuntu.ru/wiki/bumblebee#ubuntu_1604) серия статей по настройке Bumblebee в актуальных версиях дистрибутива.