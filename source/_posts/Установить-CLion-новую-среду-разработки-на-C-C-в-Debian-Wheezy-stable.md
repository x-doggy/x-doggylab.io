---
title: Установить CLion — новую среду разработки на C/C++ в Debian Wheezy (stable)
date: 2015-02-16 23:41:49
tags:
- linux
- ide
- clion
- debian
categories:
- Linux
- IDE
---
Для начала нужно перейти по ссылке:

[https://confluence.jetbrains.com/display/CLION/Early+Access+Program](https://confluence.jetbrains.com/display/CLION/Early+Access+Program)

и кликнуть по ссылке в разделе загрузки для Linux:

![](/img/for_posts/clion01.png)

Потом полученный архив нужно распаковать командой (и сразу удалить, чтоб не занимать на диске лишнего места):

```
sudo tar xvfz clion-*.gz -C /opt/ && rm clion-*.gz
```

Далее перейдём в каталог с установщиком:
```
cd /opt/clion/bin
```

Там установим права на запуск и запустим установочный скрипт:
```
sudo chmod +x ./clion.sh && ./clion.sh
```

Следуйте простым инструкциям по установке:

![](/img/for_posts/clion02.png)

Выбирайте второй вариант, т.к. вы устанавливаете эту среду в первый раз. Кстати, среда будет периодически обновляться и обновлять нужно так же, в том случае лучше выбрать первый вариант, чтобы имортировать ваши старые настройки.

![](/img/for_posts/clion03.png)

На этом шаге я предпочитаю выбирать тему “Darcula”, вы же выбирайте по своему вкусу.

![](/img/for_posts/clion04.png)

Здесь проверка необходимых компонентов для программирования. Проверьте, чтобы везде стояли галочки напротив нужных компонентов (`make`, `cc`, `c++`, `gdb`).

![](/img/for_posts/clion05.png)

Здесь ничего не меняем, просто продолжаем.

![](/img/for_posts/clion06.png)

И здесь тоже.

![](/img/for_posts/clion07.png)

Я предпочитаю создавать значок запуска в системе.

![](/img/for_posts/clion08.png)

Ну вот, этап основной настройки пройден! Далее уже запустится сам CLion с таким окошком.

![](/img/for_posts/clion09.png)

Теперь можно создать новый проект... и начать программировать!

![](/img/for_posts/clion10.png)

Вот так выглядит среда CLion с советами, я их всегда отключаю.

Ну вот мы и установили CLion! Это крайне удобная и мощная среда, она мне нравится! Удачи в программировании в ней!