---
title: "Мои любимые книги по JavaScript, которые я порекомендую"
date: 2021-11-21 13:54:02
tags:
---

### [JavaScript: The Definitive Guide](https://www.oreilly.com/library/view/javascript-the-definitive/9781491952016/)

![](/img/for_posts/javascript_the_definitive_guide.png)

Эта книга - самая настоящая библия разработки на JavaScript. Первое ее издание было в 2000-м году и на данный момент переживает уже 7-е издание. По содержанию она охватывает все аспекты JavaScript и в т.ч. фичи недавних релизов. Очень рекомендую к прочтению.

### [JavaScript: The Good Parts](https://www.amazon.com/dp/0596517742/wrrrldwideweb)

![](/img/for_posts/javascript_the_good_parts.png)

[Дуглас Крокфорд](https://www.crockford.com) - мой любимый автор, я люблю его книги, я пристально слушаю его выступления и доклады на YouTube. Когда я в том числе смотрю [его гитхаб](https://github.com/douglascrockford), я говорю себе: "Наконец-то нормальный код, не то что этот ваш PassportJS". А эта книга позволит увидеть как просто и элегантно можно решать задачи, для которых JavaScript очень хорошо подходит. У автора есть вкус в программировании и в этой книге он его передал. Горячо рекомендую.

### [Рефакторинг кода на JavaScript](https://martinfowler.com/books/refactoring.html)

![](/img/for_posts/fowler_refactoring_javascript.jpg)

В мире программирования есть по крайней мере два "Мартина": Мартин Фаулер и Роберт Мартин (у одного "Мартин" - это имя, а у другого - фамилия). Роберт Мартин хорош, он написал полезные "[Clean Code](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)" и "[Clean Architecture](https://www.amazon.com/Clean-Architecture-Craftsmans-Software-Structure/dp/0134494164)", но Фаулер куда лучше. В этой книге автор классифицировал все способы рефакторинга кода, привел несложные примеры и объяснил мотивацию к их применению.

### [Essential TypeScript](https://www.amazon.com/Essential-TypeScript-Beginner-Adam-Freeman/dp/148424978X)

![](/img/for_posts/essential_typescript.png)

Книга неожиданно легко читается, использует несложные примеры и охватывает весь спектр возможностей TypeScript. Одна из доступных книг для чтения и понимания. Рекомендую.

### [Node Cookbook](https://www.amazon.com/Node-Cookbook-techniques-server-side-development-dp-1838558756/dp/1838558756/ref=dp_ob_title_bk)

![](/img/for_posts/node_cookbook.png)

Книга докапывается до самой сути вещей, показывает тонкости, о которых не принято задумываться, расширяет сознание и позволяет узреть как разрабатывать на NodeJS безопасно.
