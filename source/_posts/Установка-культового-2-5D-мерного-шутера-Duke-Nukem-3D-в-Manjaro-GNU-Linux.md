---
title: 'Установка культового 2,5D-мерного шутера Duke Nukem 3D в Manjaro GNU/Linux'
date: 2015-07-27 13:58:47
tags:
- duke nukem
- linux
categories:
- Linux
- Games
---
Про историю игры Duke Nukem 3D и движка Build, на котором он построен, лучше всего расскажет Дмитрий Бачило:

<div style="text-align: center;">
{% iframe https://www.youtube.com/embed/VavpnVmMhgo 720 405 %}
</div>

Чтобы установить эту игру в Manjaro, для начала нужно поставить EDuke32 — кросс-платформенный вариант движка Build для Win, OS X и Linux. Для [Arch](https://www.archlinux.org/) и [Manjaro](https://manjaro.github.io/) он есть в [AUR](https://aur.archlinux.org/), поэтому поставить его через [йогурт](https://archlinux.fr/yaourt-en) не составит труда:

```!sh
yaourt -S eduke32
```

(каждое действие, которое просят подтвердить — подтверждайте!)
Пробный запуск (если запустилось, то ОК):

![](/img/for_posts/eduke32_engine.png)

В домашней директории создалась поддиректория _.eduke32_, где и будет хранится всё, что нужно для запуска игры. Создадим где-нибудь в домашней папке временную подпапку _~/d/_ для некоторых манипуляций.

Скачаем продукты жизнедеятельности игры и распакуем их заодно:

```
mkdir ~/d/ && cd ~/d/ && wget ftp://ftp.3drealms.com/share/3dduke13.zip && unzip 3dduke13.zip
```

Установим также Dosbox и запустим _INSTALL.EXE_, чтобы вытащить необходимые ресурсы игры:

```
sudo pacman -S dosbox && dosbox INSTALL.EXE
```

(просто нажимайте Enter, а как появится чёрная консоль, наберите команду _exit_).

Теперь в директории _d_ появилась поддиректория _DUKE3D_, в котором находится файлик _DUKE3D.GRP_. Его нужно скопировать в папку с игрой:

```
cp ~/d/DUKE3D/DUKE3D.GRP ~/.eduke32/
```

А папка _d_ уже не нужна, её можно удалить:

```
rm -r ~/d/
```

Всё! Теперь игруля должна запускаться командой в терминале: _eduke32_:

![](/img/for_posts/eduke32_manjaro_exported.png)

Ну и напоследок демонстрация эффекта рыбьего глаза, о котором говорил Дмитрий:

<div style="text-align: center;">
{% iframe https://www.dailymotion.com/embed/video/x4927wa 720 405 %}
</div>

За основу взята статья: [http://ubuntu-desktop.ru/?p=1767](http://ubuntu-desktop.ru/?p=1767)