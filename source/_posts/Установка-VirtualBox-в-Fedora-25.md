---
title: Установка VirtualBox в Fedora 25
date: 2017-06-12 15:15:02
tags:
- linux
- virtualbox
- fedora
categories:
- Linux
---

Некоторые вещи в Fedora стоит устанавливать с официальных сайтов, а не с помощью репозиториев. Среди них — [VirtualBox](https://www.virtualbox.org/wiki/Linux_Downloads). Я покажу как правильно произвести его установку.

Все действия будем делать из-под суперпользователя:

```
sudo -s
```

Установим первым делом необходимые зависимости:

```
dnf install binutils qt gcc make patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel dkms
```

Добавим правильный репозиторий:

```
cd /etc/yum.repos.d/
wget http://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo
```

Установим сам пакет:

```
dnf install VirtualBox-5.1
```

Теперь необходимо, чтобы были пересобраны модули ядра для поддержки виртуалбокса:

```
/usr/lib/virtualbox/vboxdrv.sh setup
```

И добавим напоследок свего пользователя в группу `vboxusers`:

```
usermod -a -G vboxusers `whoami`
```

Теперь стартуем виртуалбокс из меню и всё должно работать!