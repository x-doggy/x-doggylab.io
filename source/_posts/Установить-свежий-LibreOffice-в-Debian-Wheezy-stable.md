---
title: Установить свежий LibreOffice в Debian Wheezy (stable)
date: 2015-01-31 20:17:24
tags:
- libreoffice
- debian
- linux
categories:
- Linux
---
По умолчанию в штатной установке Дебиана идёт древний LibreOffice версии 3, я желал установить свежий (на момент публикации версия была 4.3.3.2).

Как оказалось, офис устанавливается просто, без подключения репозиториев сторонних разработчиков, а лишь «родным» из backports.

Для начала необходимо удалить старую версию:

```
sudo aptitude purge libreoffice* && apt-get autoremove
```

Далее надо прописать backports-репозиторий дебиана в список репозиториев:

```
sudo sh -c "echo 'deb http://ftp.debian.org/debian/ wheezy-backports main contrib non-free' && /etc/apt/sources.list"
```

```
sudo sh -c "echo 'deb-src http://ftp.debian.org/debian/ wheezy-backports main contrib non-free' && /etc/apt/sources.list"
```

```
sudo aptitude update # обновить список пакетов
```

Осталось установить LibreOffice одной командой:

```
sudo aptitude -t wheezy-backports install libreoffice libreoffice-help-ru libreoffice-l10n-ru
```

Вот и всё!