---
title: 'NativeScript + Angular: Введение'
date: 2020-05-14 15:09:15
tags:
---

Этот пост посвящен введению в платформу Angular и NativeScript.

NativeScript позволит вам писать мобильные приложения для Android и iOS на языке (Type / Java)Script.

Angular позволит составить приложения из компонентов, которые по своей сути будут самыми объекто-ориентированными среди конкурентов.

Подробнее введение в Angular читайте в блоге моего друга Димаса: [https://dshevchenkoo.gitlab.io/2020/05/05/intro-angular/](https://dshevchenkoo.gitlab.io/2020/05/05/intro-angular/).

Для начала убедимся, что в системе уже стоит NodeJS версии 13. Почему именно 13-я версия? Да потому что NativeScript с 14-ой версией не умеет работать, а во вторых предыдущие версии устарели. Ну ОК, можно поставить и 12-ю, потому что она на данный момент LTS.

Проверяем версию NodeJS:

```bash
$ node -v
v13.14.0
```

Теперь глобально установим пакет `nativescript`, который будет основным инструментом командной строки для работы с NativeScript. Параллельно установим `@angular/cli`, чтобы можно было также работать с Angular из командной строки, `TypeScript`, потому что тупо полезно чтобы он стоял, и еще один пакет, который просто нужен.

```bash
npm i -g nativescript typescript @angular/cli @nativescript/schematics
```

Всё, после установки `nativescript` стала доступна команда `tns` ("Telerik NativeScript"), при помощи которой будем создавать проект.

Убедимся в его наличие через вывод версии `tns`:

```bash
$ tns --version
6.5.0
```

Отлично! Теперь можем создать приложение. Всего-то нужно перейти в нужную папку и набрать следующую команду:

```bash
tns create <имя приложения> --ng
```

Пропишите свое имя приложения. После нескольких минут ожиданий вы увидите новую папку с названием приложения. Зайдём в неё.

Уже внутри папки мы видим файл `angular.json`, значит, это то, что надо, всё создалось правильно.

Теперь убедимся, что в системе есть всё, чтобы запустить приложения на девайсе (на реальном или на эмуляторе - не важно!).

Изнутри папки проекта пропишем замечательную команду `doctor`:

```bash
$ tns doctor
```

Эта команда проверит наличие JDK в системе, как настроен SDK и какие есть препятствия запуска приложения. В моем случае всё должно быть ОК:

```
✔ Getting environment information 

No issues were detected.
✔ Your ANDROID_HOME environment variable is set and points to correct directory.
✔ Your adb from the Android SDK is correctly installed.
✔ The Android SDK is installed.
✔ A compatible Android SDK for compilation is found.
✔ Javac is installed and is configured properly.
✔ The Java Development Kit (JDK) is installed and is configured properly.
✔ Local builds for iOS can be executed only on a macOS system. To build for iOS on a different operating system, you can use the NativeScript cloud infrastructure.
✔ Getting NativeScript components versions information...
✔ Component nativescript has 6.5.0 version and is up to date.
```

Действительно так.

Проверять приложение будем через эмулятор. Можно использовать как встроенный в Android SDK инструмент, так и Genymotion, который я предпочитаю. Запустим устройство с Android в строй.

Убедимся, что девайс доступен для NativeScript:

```bash
$ tns devices
```

И в таблице мы видим все Android-устройства, подключенные к компьютеру.

```
Connected devices & emulators
Searching for devices...
iTunes is not available for this operating system. You will not be able to work with connected iOS devices.
┌───┬───────────────────┬──────────┬─────────────────────┬──────────┬───────────┬─────────────────┐
│ # │ Device Name       │ Platform │ Device Identifier   │ Type     │ Status    │ Connection Type │
│ 1 │ Samsung Galaxy S2 │ Android  │ 192.168.58.107:5555 │ Emulator │ Connected │ Local           │
└───┴───────────────────┴──────────┴─────────────────────┴──────────┴───────────┴─────────────────┘
```

Из таблицы видим, что нужное мне устройство стоит под номером `1`. Зная это, ничего не мешает нам запустить, наконец, проект:

```bash
$ tns run android --device 1
```

Иииии - пошла сборка и проект успешно стартанул! То, чего мы и добивались!

Успешного изучения этих технологий!
