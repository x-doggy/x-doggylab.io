---
title: Генератор паролей
date: 2017-06-17 16:54:12
---

<style>
.password-wrap {
    display: flex;
    justify-content: space-between;

}

.password-refresh-btn {
    flex-basis: 100px;
}

#password-pass {
    width: 100%;
    text-align: center;
    font-family: "Ubuntu Mono", "Courier New", monospace;
    font-size: 3em;
    font-weight: bold;
}
</style>

<div class="password-wrap">
    <div id="password-pass"></div>
    <button class="password-refresh-btn" onclick="refresh_password_generation();">Ещё</button>
</div>

<script>
function generate_password() {
    function randint(a, b) {
        return Math.floor(a + Math.random() * (b - a + 1));
    }
    
    var CHEM = [
                "He", "Li", "Be", "Ne", "Na", "Mg", "Al", "Si", "Cl",
                "Ar", "Ca", "Sc", "Ti", "Cr", "Mn", "Fe", "Co", "Ni",
                "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb",
                "Sr", "Zr", "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag",
                "Cd", "In", "Sn", "Sb", "Te", "Xe", "Cs", "Ba", "La",
                "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy",
                "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "Re", "Os",
                "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At",
                "Rn", "Fr", "Ra", "Ac", "Th", "Pa", "Np", "Pu", "Am",
                "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr", "Ku"
                ];
    
    return CHEM[ Math.floor(Math.random() * CHEM.length) ] +
           "OH" +
           randint(10, 99) +
           String.fromCharCode( randint(97, 122) ) +
           String.fromCharCode( randint(97, 122) );
}

function refresh_password_generation() {
    document.getElementById('password-pass').innerHTML = generate_password();
}

refresh_password_generation();
</script>