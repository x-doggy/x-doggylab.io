---
title: About me
date: 2020-05-03 16:13:07
---

Hello, I am Vladimir Stadnik - a web developer from Omsk, Russia.

I was started to learn web since I was 13. HTML was the first language I do understand, it seemed to be so easy. I liked it immediately.

After about a month, I knew about JavaScript and try to learn it.

JavaScript had a big influence to me. It seemed so logical, I started to write a simple scripts in Notepad, running under Windows XP, and execute it in Internet Explorer 6 browser.
Every line that I wrote worked as I predict, it was so cool and then I understand that I can give some commands to a script in a browser and someone runner will do it as I say to it.

Periodically I am [writing](https://codepen.io/x-doggy/) some things that coming up from myself.
And most of my spare time from school, I create more and more web pages for practise.

I've been led on frontend things all that time since 2009 to 2012 - talking about CSS and JavaScript differences in his API on IE browser and others.

In the middle of 2012 I tried unsuccessfully to learn PHP, because my Internet connection was so poor at that time, and I was not able to read or download any PHP tutorial.

Later on, Internet at my house was grow up, and I was able to read about PHP, reviewing PHP 5 sources from random resources.

PHP was easy to me, and I can create my own [blog](https://gitlab.com/x-doggy/php-blog) for 2 days.

A year later, I graduated from school and became a student at Omsk State University, applied mathematics and IT speciality.

During that time I learn both C++ and Java, using Linux, and occasionally knew about NodeJS. It was so cool and amazing! The thing that can help to write console programs in JS - that's astounding!

All my attention switched to NodeJS, I understand how to work with it so quick! 

Since that time I love to write programs both in pure JS, and in NodeJS.

Now I happily work with JavaScript, NodeJS, TypeScript and with frameworks such as Vue, React, React Native and NestJS.
I believe that every new project must be written in TypeScript, because it helps in so many cases write code the right way.
