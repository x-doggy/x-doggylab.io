# hexo-theme-material-indigo-en

Material Design Hexo theme, based on Hexo 3.0+ production. [Preview](http://x-doggy.gitlab.io/)

Rewritten in English because of initial Chinese. Originally took from [https://github.com/yscoder/hexo-theme-indigo](https://github.com/yscoder/hexo-theme-indigo)
